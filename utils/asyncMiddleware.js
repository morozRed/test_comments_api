'use strict';

const asyncMiddleware = (fn) =>
    (req, res, next) => {
        Promise.resolve(fn(req, res, next))
            .catch((err) => {
              if (process.env.development) {
                console.log(err);
              }
              res.status(400).json({ error: 'Bad request!'  });
            });
    };

module.exports = asyncMiddleware;