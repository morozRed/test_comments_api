
## Comments API

Example API implementation of tree structured comments.

###  Methods

#### Auth

Use this method to get auth token and then use it in header.

Example:
```
Authorization:Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVhYmU1ZjBhODAxNjIzNWU3OTE2Y2ZiOCIsImVtYWlsIjoidGVzdEBnbWFpbC5jb20iLCJwYXNzd29yZCI6IiQyYSQxMCRwTC5tTmlOV2RUOVFXVjA1cC5OZEllL2s4Nk1OV3Vic3pCRFdzVWo3T1Vub0pDdC9WRFpuMiIsInVzZXJuYW1lIjoiU3VwZXJUZXN0IiwiX192IjowfSwiaWF0IjoxNTIyNzM4OTMwfQ.46ddcnsA5MIp3yZlao8eQvbwsH7OUHB46QGtJoCOCQw
```
`[POST] localhost:3000/api/sessions`

#### Create user:
`[POST] localhost:3000/api/users`

Body params:
```
username: example_username
email: example@example.com
password: 123456
```
#### Get all users [Auth required]
Will return list of all users

`[GET] localhost:3000/api/users`

#### Get comments [Auth required]
`[GET] localhost:3000/api/comments`
#### Create comment [Auth required]
 `[POST] localhost:3000/api/comments`
 
Body params:
```
text: example comment
parentCommentId: 5ac35da6e35933bff1c7f77c
```

> If `parentCommentId` param is not provided comment will be created on
> root level.

#### Get max nested level in comments tree [Auth required]
`[GET] localhost:3000/api/comments/max-nesting-level`