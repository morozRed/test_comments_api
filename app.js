

const express = require('express');
const helmet = require('helmet');

const app = express();
const passport = require('passport');

const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(helmet());

// routes
app.use('/api', require('./api'));

require('./common/mongodb.js');
require('./common/passport.js');

app.use(passport.initialize());

// start server
app.listen(port, () => {
  console.log(`API listening on port: ${port}`);
});

module.exports = app;
