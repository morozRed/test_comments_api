

const teamService = {
  sortByRank: teams => teams.sort((t1, t2) => t1.rank - t2.rank),
  getAvgRank: (teams) => {
    if (teams.length === 0) return 0;
    const sum = teams.reduce((avg, team) => avg + team.rank, 0);
    return sum / teams.length;
  },
};

/**
  Test data below
*/
const teams = [
  { name: 'qwe1', rank: 51 },
  { name: 'qwe2', rank: 22 },
  { name: 'qwe3', rank: 92 },
];

const avgRank = teamService.getAvgRank(teams);
const sortedTeams = teamService.sortByRank(teams);

