

const express = require('express');
const passport = require('passport');

const router = express.Router();

function ensureAuth() {
  return passport.authenticate('jwt', { session: false });
}

router.use('/users', require('./users'));
router.use('/comments', ensureAuth(), require('./comments'));
router.use('/sessions', require('./sessions'));

module.exports = router;
