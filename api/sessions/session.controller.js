

const bcrypt = require('bcrypt');
const config = require('config');
const jwt = require('jsonwebtoken');
const asyncMiddleware = require('../../utils/asyncMiddleware');
const User = require('../users/user.model');

module.exports = {
  create: asyncMiddleware(async (req, res) => {
    const user = await User.findOne({ email: req.body.email });
    if (!user) res.status(404).json({ error: 'No user found!' });
    const isPasswordValid = await bcrypt.compare(req.body.password, user.password);
    if (isPasswordValid) {
      const token = jwt.sign({ user }, config.get('Passport.secret'));
      res.status(200).json({ user, token });
    } else {
      res.status(403).json({ message: 'Forbidden' });
    }
  }),
};
