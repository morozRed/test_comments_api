

/* global before, beforeEach, it, describe */

const Comment = require('./comment.model');
const User = require('../users/user.model');
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../app');

const { expect } = chai.expect;

chai.use(chaiHttp);

let accessToken = '';

async function addCommentsToDb() {
  const comment1 = await new Comment({ text: 'test' }).save();
  const comment2 = await new Comment({ text: 'test', parent: comment1.id }).save();
  const comment3 = await new Comment({ text: 'test', parent: comment2.id }).save();

  await comment1.update({ child: comment2.id });
  await comment2.update({ child: comment3.id });
  return [comment1, comment2, comment3];
}

before(async (done) => {
  await User.remove({});
  const testUser = new User({
    username: 'test',
    email: 'test@gmail.com',
    password: '123456',
  });

  await testUser.save();
  chai.request(app)
    .post('/api/sessions')
    .send({
      email: testUser.email,
      password: '123456',
    })
    .end((err, res) => {
      if (err) done(err);
      accessToken = res.body.token;
      done();
    });
});

describe('Comments', () => {
  beforeEach((done) => {
    Comment.remove({}, (err) => {
      done(err);
    });
  });
  describe('/POST comment', () => {
    it('it should create new comment', (done) => {
      const testComment = new Comment({
        text: 'Test comment',
      });
      chai.request(app)
        .post('/api/comments')
        .send(testComment)
        .set('Authorization', `Bearer ${accessToken}`)
        .end((err, res) => {
          if (err) done(err);
          expect(res).have.status(200);
          expect(res.body).to.be.a('object');
          done();
        });
    });
    it('it should not create new comment without text', (done) => {
      const testComment = new Comment({});
      chai.request(app)
        .post('/api/comments')
        .send(testComment)
        .set('Authorization', `Bearer ${accessToken}`)
        .end((err, res) => {
          if (err) done(err);
          expect(res).to.have.status(400);
          done();
        });
    });
  });
  describe('/GET comments', () => {
    it('it should return the list of comments', async (done) => {
      await new Comment({
        text: 'test0',
      }).save();
      chai.request(app)
        .get('/api/comments')
        .set('Authorization', `Bearer ${accessToken}`)
        .end((err, res) => {
          if (err) done(err);
          expect(res).to.have.status(200);
          expect(res.body).to.be.a('array');
          expect(res.body).to.have.lengthOf(1);
          done();
        });
    });
  });

  describe('/GET comments/max-nesting-level', () => {
    it('it should return 0 nesting level for empty comments', async (done) => {
      await addCommentsToDb();
      chai.request(app)
        .get('/api/comments/max-nesting-level')
        .set('Authorization', `Bearer ${accessToken}`)
        .end((err, res) => {
          if (err) done(err);
          expect(res).to.have.status(200);
          expect(res.body).to.be.a('object');
          expect(res.body).to.have.property('maxNestingLevel', 2);
          done();
        });
    });
  });
});
