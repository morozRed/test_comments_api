

const mongoose = require('mongoose');

const { Schema } = mongoose;

const CommentSchema = new Schema({
  text: {
    type: String,
    required: 'Comment text can not be emty',
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  child: {
    type: Schema.Types.ObjectId,
    ref: 'Comment',
  },
  parent: {
    type: Schema.Types.ObjectId,
    ref: 'Comment',
  },
});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;
