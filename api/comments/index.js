
const express = require('express');

const router = express.Router();
const controller = require('./comment.controller');

router
  .post('/', controller.create)
  .get('/', controller.getAll)
  .get('/max-nesting-level', controller.getMaxNestingLevel);

module.exports = router;
