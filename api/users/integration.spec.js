

/* global beforeEach, it, describe */

const User = require('./user.model');
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../app');

const { expect } = chai.expect;

chai.use(chaiHttp);

describe('Users', () => {
  beforeEach((done) => {
    User.remove({}, (err) => {
      if (err) done(err);
      done();
    });
  });
  describe('/POST user', () => {
    it('it should create new user', (done) => {
      const testUser = new User({
        username: 'test',
        email: 'test@email.com',
        password: '123456',
      });
      chai.request(app)
        .post('/api/users')
        .send(testUser)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.a('object');
          expect(res.body).to.have.property('username');
          expect(res.body).to.have.property('email');
          expect(res.body).to.have.property('password');
          done();
        });
    });
  });
});
