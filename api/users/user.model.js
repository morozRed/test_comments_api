

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const { Schema } = mongoose;

const UserSchema = new Schema({
  email: {
    type: String,
    required: 'User email can not be emty',
    unique: 'User with this email already exists',
  },
  password: {
    type: String,
    required: 'User password can not be emty',
  },
  username: {
    type: String,
    required: 'User name can not be empty',
    unique: 'User with this name already exists',
  },
});

UserSchema.pre('save', async function hashPassword(next) {
  const user = this;
  try {
    const hashedPassword = await bcrypt.hash(user.password, 10);
    user.password = hashedPassword;
    next();
  } catch (err) {
    next(err);
  }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
