

const asyncMiddleware = require('../../utils/asyncMiddleware');
const User = require('./user.model');

module.exports = {
  create: asyncMiddleware(async (req, res) => {
    const newUser = new User(req.body);
    const savedUser = await newUser.save();
    res.status(200).json(savedUser);
  }),
  getAll: asyncMiddleware(async (req, res) => {
    const users = await User.aggregate([
      {
        $lookup: {
          from: 'comments',
          localField: '_id',
          foreignField: 'author',
          as: 'posted_comments',
        },
      },
      {
        $group: {
          _id: {
            username: '$username',
            email: '$email',
            total_comments: { $size: '$posted_comments' },
          },
        },
      },
      {
        $project: {
          _id: 0,
          total_comments: '$_id.total_comments',
          username: '$_id.username',
          email: '$_id.email',
        },
      },
      {
        $sort: {
          total_comments: -1,
        },
      },
    ]);
    res.status(200).json(users);
  }),
};
